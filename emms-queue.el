;;; emms-queue.el --- Playlist queue handling for EMMS.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; emms-queue.el adds track queueing support to Emms.  To use it add:
;;   (add-to-list 'load-path "path/to/emms-queue/")
;;   (require 'emms-queue)
;; then set either of the following:
;;   (setq emms-player-next-function 'emms-queue-or-next)
;; or
;;   (setq emms-player-next-function 'emms-queue-or-random)
;; in your .emacs.  `emms-queue-or-next' defaults to sequential track
;; selection when no tracks are queued, whereas `emms-queue-or-random'
;; defaults to random track selction.
;;
;; The queued state of the track under point can toggled using 'e' in the emms
;; playlist buffer.  If track information has been loaded, a group of tracks
;; containing the given regexp in the tags can be queued using 'M-x
;; emms-queue-tracks-matching-regexp'.  'E' can be used to remove all tracks
;; from the queue.
;;
;; Queueing is designed to work with any file-based player.  Other players,
;; such as MPD, are currently not supported.

;;; History:
;; 0.1.1+git
;; * Make `n' in the playlist buffer obey `emms-player-next-function'.
;; 0.1.1
;; * Renamed various functions for consistency.
;; * Now requires `emms-playlist-mode'.
;; * Fix a bug where tracks already queued would be removed when queueing via
;;   regexps.
;; * No longer set the overlay face for the next non-queued track.
;; 0.1
;; * Initial version

;;; Code:
(require 'emms-playlist-mode)

(defvar emms-queue-version "0.1.1+git"
  "Version of emms-queue.")

(defvar emms-queue-overlay-list '()
  "Overlays for tracks to be played.")

(defface emms-queue-overlay-face
  '((((class color) (background dark))
     (:foreground "palegreen2"))
    (((class color) (background light))
     (:foreground "forest green"))
    (((type tty) (class mono))
     (:inverse-video t))
    (t (:background "green")))
  "Face used to display queued tracks"
  :group 'emms-queue)

(define-key emms-playlist-mode-map "e" 'emms-queue-track)
(define-key emms-playlist-mode-map "E" 'emms-queue-clear)


;;; Overlays
(defun emms-queue-add-track-overlay (&optional hide)
  "Place the track queue overlay for the current track."
  (let ((overlay (make-overlay (save-excursion (beginning-of-line) (point))
                               (save-excursion (end-of-line) (point)))))
    (unless hide
      (overlay-put overlay 'face 'emms-queue-overlay-face))
    (overlay-put overlay 'name 'emms-queue-track)
    overlay))

(defun emms-queue-remove-track-overlay ()
  "Remove overlays beginning at `point'.
Return non-nil when an overlay has been removed."
  (let ((overlay-list emms-queue-overlay-list)
        (overlay-at-point nil))
    (while overlay-list
      (when (= (overlay-start (car overlay-list)) (point))
        (setq emms-queue-overlay-list
              (remove (car overlay-list) emms-queue-overlay-list)
              overlay-at-point t))
      (setq overlay-list (cdr overlay-list)))
    (when overlay-at-point
      (remove-overlays (point) (save-excursion (end-of-line) (point))
                       'name 'emms-queue-track)
      t)))


;;; Track queueing
(defun emms-queue-add-suitable-tracks (artist album title)
  "Queue the track at point if it corresponds to the criteria.
ARTIST, ALBUM and TITLE are the regexps of the artist, album and
title of the track to match, respectively."
  (when (emms-queue-matched-fields
         (emms-playlist-track-at (point)) artist album title)
    (emms-queue-track 1 t))
  ;; goto next track
  (= 0 (forward-line 1)))

(defun emms-queue-matching-tracks (artist album title)
  "Queue tracks in the buffer matching REGEXP.
ARTIST, ALBUM and TITLE are strings representing the artist,
album and title to search for."
  (interactive "sArtist: \nsAlbum: \nsTitle: ")
  (save-excursion
    (set-buffer emms-playlist-buffer)
    (goto-char (point-min))
    (while (emms-queue-add-suitable-tracks artist album title))))

(defun emms-queue-matched-fields (track &rest fields)
  "Return non-nil if TRACK corresponds to FIELDS.
TRACK is the track to check, and FIELDS is the list of fields to
compare to track.  FIELDS should consist of the artist, album and
title."
  (let ((data (mapcar (lambda (field) (emms-track-get track field))
                      '(info-artist info-album info-title)))
        (count -1)
        (match t))
    ;; Check each of artist, album and title and indicate whether the track
    ;; matches each specified field.
    (while (< (setq count (1+ count)) 3)
      (when (or (and (nth count data)
                     (not (string-match (nth count fields) (nth count data))))
                (and (not (nth count data))
                     (not (string= "" (nth count fields)))))
        (setq match nil)))
    match))

(defun emms-queue-next-sequential-track ()
  "Return the track would usually be played next in sequence.
When beginning to play tracks from the queue with
`emms-queue-or-next', queue the next sequential track so that
play will resume from the same position in the playlist."
  (when (and (eq emms-player-next-function 'emms-queue-or-next)
             (not emms-queue-overlay-list))
    (save-excursion
      (goto-char (overlay-start emms-playlist-mode-selected-overlay))
      (forward-line 1)
      (emms-queue-add-track-overlay t))))

(defun emms-queue-target ()
  "Queue the track at `point'.
Append the track at point, adjusting the existing queue as necessary.
This will sort the queue so that the non-queued track is always last."
  (setq
   emms-queue-overlay-list
   (nreverse
    (if (eq emms-player-next-function 'emms-queue-or-next)
        (let ((float-track (car (reverse emms-queue-overlay-list))))
          (cons (or float-track (emms-queue-next-sequential-track))
                (cons (emms-queue-add-track-overlay)
                      (cdr (nreverse emms-queue-overlay-list)))))
      (cons (emms-queue-add-track-overlay) emms-queue-overlay-list)))))

(defun emms-queue-track (count &optional preserve)
  "Toggle the track at `point' from the queue.
If PRESERVE is non-nil, do not remove the track from the queue."
  (interactive "p")
  (while (> count 0)
    (save-excursion
      (beginning-of-line)
      (let ((was-member (emms-queue-remove-track-overlay)))
        (when (and emms-playlist-mode-selected-overlay
                   ;; Don't queue the track currently playing
                   (/= (save-excursion (beginning-of-line) (point))
                       (overlay-start emms-playlist-mode-selected-overlay))
                   (or (not was-member) (and preserve was-member))
                   (emms-track-p (emms-playlist-track-at (point))))
          (emms-queue-target))))
    ;; Move to next track or end of buffer, whichever comes next
    (while (and (eq (forward-line 1) 0)
                (not (emms-track-p
                      (emms-playlist-current-selected-track)))))
    (setq count (1- count))))

(defun emms-queue-clear ()
  "Clear the queue."
  (interactive)
  (remove-overlays (point-min) (point-max) 'name 'emms-queue-track)
  (setq emms-queue-overlay-list nil))


;;; Next functions
(defun emms-queue-play-next-queued ()
  "Select the next track in the queue."
  (emms-playlist-select
   (let ((overlay (pop emms-queue-overlay-list)))
     (when overlay
       (prog1
           (overlay-start overlay)
         (delete-overlay overlay)))))
  (emms-start))

(defmacro define-emms-queue-player-function (name docstring default-body)
  "Define an emms-queue function for use with `emms-player-next-function'.
Where, NAME is the name of the function to define and
DEFAULT-BODY is the body of the fall-back action when no queue is
available."
  `(defun ,name ()
     ,docstring
     (with-current-emms-playlist
       (if emms-player-playing-p
           (emms-stop))
       (cond
        (emms-repeat-track
         (emms-start))
        (emms-queue-overlay-list
         (emms-queue-play-next-queued))
        (t
         ,default-body)))))

(defun emms-queue-goto-next-track ()
  "Play the next track in the playlist."
  (interactive)
  (funcall emms-player-next-function))

(define-key emms-playlist-mode-map "n" 'emms-queue-goto-next-track)

(define-emms-queue-player-function emms-queue-or-random
  "Select the next track in the queue.
Designed for use with `emms-player-next-function'."
  (emms-random))

(define-emms-queue-player-function emms-queue-or-next
  "Play the next track in the queue or the playlist.
Designed for use with `emms-player-next-function'."
  (progn
    (emms-playlist-current-select-next)
    (emms-start)))

(provide 'emms-queue)

;;; emms-queue.el ends here
